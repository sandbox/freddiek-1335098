* Clean up the .module file from hook_menu_attribute_info() and down, removing
  legacy code from the Menu Attributes module.
* Implement an easy way to choose between different overlay/dropdown options
* Remove dependency on jQuery hoverIntent and Libraries module