(function($){
  $(function(){
    $('li > a').hoverIntent({
      sensitivity: 7,
      interval: 300,
      over: showOverlay,
      timeout: 500,
      out: hideOverlay
    });
  });

  function showOverlay(){
    console.log('hej');
    $(this).parent().find('.overlay-div').css({'top': $(this).position().top + 30, 'left': $(this).position().left}).toggle();
  }

  function hideOverlay(){
    console.log('hej då');
    $(this).parent().find('.overlay-div').toggle();
    // This should be replaced with a listener for the HTML object
    // The menu and overlay panes should stop event bubbling
  }

})(jQuery);