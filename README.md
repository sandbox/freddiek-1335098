This module requires jQuery hoverIntent.

To install this library, install the Libraries module
(http://drupal.org/project/libraries) and then download hoverIntent
(http://cherne.net/brian/resources/jquery.hoverIntent.minified.js) and put it in
libraries/jquery.hoverIntent/jquery.hoverIntent.minified.js. Do note the capital
I:s in hoverIntent.